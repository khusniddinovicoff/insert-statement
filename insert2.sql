insert into actor(actor_id, first_name, last_name)
values (1012, 'Elijah', 'Wood'),
       (1014, 'Ian', 'McKellen'),
       (1017, 'Liv', 'Tyler'),
       (1019, 'Viggo', 'Mortensen');

insert into film_actor(actor_id, film_id)
VALUES (1012, 10011),
       (1014, 10011),
       (1017, 10011),
       (1019, 10011)